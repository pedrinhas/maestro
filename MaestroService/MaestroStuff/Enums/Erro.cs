﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaestroStuff.Enums
{
    public static class Erro
    {
        public enum Severidade
        {
            Info = 0,
            Warning = 1,
            Error = 2,
            Critical = 3
        }
    }
}
