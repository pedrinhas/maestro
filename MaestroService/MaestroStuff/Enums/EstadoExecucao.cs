﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaestroStuff.Enums
{
    public enum EstadoExecucao
    {
        Disponivel = 1,
        EmExecucao = 2,
        Erro = 3,
        ErroCritico = 4,
        Parado = 5
    }
}
