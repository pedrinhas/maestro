﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MaestroStuff.Notifications
{

    [KnownType(typeof(NotificationExceptionJSONObject))]
    [KnownType(typeof(NotificationInfoJSONObject))]
    [KnownType(typeof(NotificationEndJSONObject))]
    [DataContract]
    public class Notification
    {
        [DataMember]
        public NotificationType? Tipo { get; set; }
        [DataMember]
        public string Texto { get; set; }
        private DateTime DataObject { get; set; }
        [DataMember]
        public string Data { get { return DataObject.ToString("yyyy-MM-dd HH:mm:ss"); } }
        public string ToJSON()
        {
            return new JavaScriptSerializer().Serialize(this);
        }

        public static Notification FromJSON(string json)
        {
            try
            {
                var res = new JavaScriptSerializer().Deserialize<Notification>(json);
                if (res != null)
                    switch (res.Tipo)
                    {
                        case NotificationType.Exception:
                            res = new JavaScriptSerializer().Deserialize<NotificationExceptionJSONObject>(json);
                            break;
                        case NotificationType.Info:
                            res = new JavaScriptSerializer().Deserialize<NotificationInfoJSONObject>(json);
                            break;
                        case NotificationType.Start:
                            res = new JavaScriptSerializer().Deserialize<NotificationStartJSONObject>(json);
                            break;
                        case NotificationType.End:
                            res = new JavaScriptSerializer().Deserialize<NotificationEndJSONObject>(json);
                            break;
                    }

                return res;
            }
            catch
            {
                return null;
            }
        }

        public Notification()
        {
            DataObject = DateTime.Now;
        }
    }
}
