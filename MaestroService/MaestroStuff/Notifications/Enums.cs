﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MaestroStuff.Notifications
{
    public enum NotificationType
    {
        Info,
        Exception,
        Start,
        End
    }
}
