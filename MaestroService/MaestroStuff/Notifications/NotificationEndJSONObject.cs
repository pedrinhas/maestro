﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaestroStuff.Notifications
{
    public class NotificationEndJSONObject : Notification
    {
        public NotificationEndJSONObject() : base()
        {
            this.Tipo = NotificationType.End;
        }
        public NotificationEndJSONObject(string comment) : this()
        {
            this.Texto = comment;
        }
    }
}
