﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MaestroStuff.Notifications
{
    [DataContract]
    public class NotificationStartJSONObject : Notification
    {
        public NotificationStartJSONObject() : base()
        {
            this.Tipo = NotificationType.Start;
        }
        public NotificationStartJSONObject(string text)
            : this()
        {
            this.Texto = text;
        }
    }
}
