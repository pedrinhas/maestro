﻿using MaestroStuff.Notifications;
using MaestroStuff.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace MaestroStuff.Notifications
{
    public static class MaestroNotifier
    {
        /// <summary>
        /// Sem problemas de concorrência, esta classe pode ser static
        /// </summary>
        public static class ConsoleNotifier
        {
            public static void WriteToConsole(string text)
            {
                Console.OutputEncoding = Encoding.Default;
                Console.WriteLine(text.Trim(new char[] { '\n', '\r' }));
            }
            public static void WriteToConsole(Exception ex, Erro.Severidade sev = Erro.Severidade.Error)
            {
                var n = new NotificationExceptionJSONObject(ex, sev);

                WriteToConsole(n.ToJSON());
            }
            public static void WriteToConsole_Info(string text)
            {
                var n = new NotificationInfoJSONObject(text);
                var json = n.ToJSON();

                WriteToConsole(json);
            }
            public static void WriteToConsole_Start(string comment)
            {
                var n = new NotificationStartJSONObject(comment);
                var json = n.ToJSON();

                WriteToConsole(json);
            }
            public static void WriteToConsole_End(string comment)
            {
                var n = new NotificationEndJSONObject(comment);
                var json = n.ToJSON();

                WriteToConsole(json);
            }
        }

        /// <summary>
        /// Criar objeto para não haver cross log com outras instâncias
        /// </summary>
        public class WebServiceNotifier
        {
            public string CompleteLog { get; private set; }

            public void Log(string text)
            {
                CompleteLog = string.Format("{1}{0}{2}", Environment.NewLine, CompleteLog, text);
            }
            public void Log(Exception ex, Erro.Severidade sev = Erro.Severidade.Error)
            {
                var n = new NotificationExceptionJSONObject(ex, sev);

                Log(n.ToJSON());
            }
            public void Log_Info(string text)
            {
                var n = new NotificationInfoJSONObject(text);

                Log(n.ToJSON());
            }

            /// <summary>
            /// Escreve o log para a resposta do pedido. Chamar no fim do método. Deve-se tratar alguma Exception que o método lance
            /// </summary>
            /// <param name="responseStream"></param>
            public void WriteLogToResponse(Stream responseStream)
            {
                using(var writer = new StreamWriter(responseStream))
                {
                    writer.Write(CompleteLog);
                }
            }
        }
    }
}
