﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MaestroStuff.Notifications
{
    [DataContract]
    public class NotificationInfoJSONObject : Notification
    {
        public NotificationInfoJSONObject() : base()
        {
            this.Tipo = NotificationType.Info;
        }
        public NotificationInfoJSONObject(string text) : this()
        {
            this.Texto = text;
        }
    }
}
