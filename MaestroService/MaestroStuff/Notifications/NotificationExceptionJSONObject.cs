﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MaestroStuff.Enums;
using System.Web.Script.Serialization;

namespace MaestroStuff.Notifications
{
    [DataContract]
    public class NotificationExceptionJSONObject : Notification
    {
        public NotificationExceptionJSONObject() : base()
        {
            this.Tipo = NotificationType.Exception;
        }
        public NotificationExceptionJSONObject(Exception ex, Erro.Severidade sev = Erro.Severidade.Error) : this()
        {
            this.Class = ex.GetType().ToString();
            this.Message = ex.Message;
            this.StackTrace = ex.StackTrace;
            this.TargetSite = ex.TargetSite.ToString();
            this.Severidade = sev;

            if(ex.InnerException != null) this.InnerException = new NotificationExceptionJSONObject(ex.InnerException);

            this.Texto = string.Format("[{0}] {1}", ex.GetType().ToString(), ex.Message);
        }
        [DataMember]
        public string Class { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }
        [DataMember]
        public string TargetSite { get; set; }
        [DataMember]
        public NotificationExceptionJSONObject InnerException { get; set; }
        [DataMember]
        public Erro.Severidade Severidade { get; set; }
    }
}
