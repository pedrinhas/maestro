﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MaestroStuff.Tools
{
    public static class Toolkit
    {
        public static class Colisoes
        {
            public static string MakeUniqueName(string name, IEnumerable<string> allNames)
            {
                string newName = name;

                for (int i = 1; ; ++i)
                {
                    if (allNames.Where(x => x == newName).Count() == 0)
                        return newName;

                    newName = string.Format("{0}_{1}", name, i);
                }
            }
            public static string MakeUniqueFileName(string name, IEnumerable<string> allNames)
            {
                string newName = name;

                for (int i = 1; ; ++i)
                {
                    if (allNames.Where(x => x == newName).Count() == 0)
                        return newName;

                    newName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(name), i, Path.GetExtension(name));
                }
            }
        }
        
        public static class Email
        {
            private static string From;
            private static string SMTPServer;

            #region init

            private static bool init = false;
            public static void Init(string smtp)
            {
                SMTPServer = smtp;

                init = true;
            }
            public static void Init(string smtp, string from)
            {
                From = from;
                Init(smtp);
            }

            #endregion

            public static bool SendEmail(string from, List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                if(!init || string.IsNullOrWhiteSpace(SMTPServer))
                {
                    return false;
                }

                try
                {
                    MailMessage mail = new MailMessage(from, to.First());
                    foreach (var toAddress in to.Where(x => x != to.First()))
                    {
                        mail.To.Add(toAddress);
                    }
                    foreach (var ccAddress in cc)
                    {
                        mail.CC.Add(ccAddress);
                    }
                    foreach (var bccAddress in bcc)
                    {
                        mail.Bcc.Add(bccAddress);
                    }
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = SMTPServer;
                    mail.Subject = subject;
                    mail.Body = body;
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
            public static bool SendEmail(List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                return SendEmail(From, to, subject, body, cc, bcc);
            }
        }
    }
}
