﻿namespace MaestroService.Aplicacoes
{
    using global::MaestroService.DB;
    using global::MaestroService.Log;
    using MaestroStuff.Enums;
    using MaestroStuff.Notifications;
    using Quartz;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Threading;

    /// <summary>
    /// Defines the <see cref="Aplicacao" />
    /// </summary>
    public class Aplicacao : IJob
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Aplicacao"/> class.
        /// </summary>
        public Aplicacao()
        {
            Args = new List<string>();
        }

        /// <summary>
        /// Gets or sets the ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the TipoAplicacao
        /// </summary>
        public Tipo TipoAplicacao { get; set; }

        /// <summary>
        /// Gets or sets the Nome
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Gets or sets the Descricao
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets or sets the Caminho
        /// </summary>
        public string Caminho { get; set; }

        /// <summary>
        /// Gets or sets the Args
        /// </summary>
        public List<string> Args { get; set; }

        /// <summary>
        /// Gets or sets the IntervaloTempo
        /// </summary>
        public string IntervaloTempo { get; set; }

        /// <summary>
        /// Gets or sets the EstadoExecucao
        /// </summary>
        public int EstadoExecucao { get; set; }

        /// <summary>
        /// Gets or sets the UltimaExecucao
        /// </summary>
        public DateTime UltimaExecucao { get; set; }

        /// <summary>
        /// Gets or sets the ProximaExecucao
        /// </summary>
        public DateTime? ProximaExecucao { get; set; }

        /// <summary>
        /// Gets or sets the DuracaoUltimaExecucao
        /// </summary>
        public TimeSpan DuracaoUltimaExecucao { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Ativo
        /// </summary>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets or sets the IDLog
        /// </summary>
        public int IDLog { get; set; }

        /// <summary>
        /// The RunWithOutput
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        private string RunWithOutput()
        {
            string allOutput = "";

            //NÃO PRECISA DE ESTAR DENTRO DE UMA THREAD PORQUE O PRINCIPAL JÁ CORRE NUMA À PARTE, NÃO BLOQUEANDO OUTRAS EXECUÇÕES
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = Caminho,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            //definir parametros
            if (Args.Count > 0)
            {
                Args.ForEach(x => proc.StartInfo.Arguments = string.Format("{0} {1}", proc.StartInfo.Arguments, x));
            }

            proc.Start();
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();

                allOutput += line + Environment.NewLine;
            }

            return allOutput;
        }

        /// <summary>
        /// The CallWebService
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        private string CallWebService()
        {
            string allOutput = "";

            //NÃO PRECISA DE ESTAR DENTRO DE UMA THREAD PORQUE O PRINCIPAL JÁ CORRE NUMA À PARTE, NÃO BLOQUEANDO OUTRAS EXECUÇÕES
            Regex reg = new Regex(@"[\w]*://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$");
            if (!reg.IsMatch(Caminho))
            {
                Caminho = string.Format("http://{0}", Caminho);
            }

            var request = (HttpWebRequest)WebRequest.Create(Caminho);
            request.KeepAlive = true;
            try
            {
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var s = new StreamReader(response.GetResponseStream()))
                {
                    if ((int)response.StatusCode < 300)
                    {
                        //OK
                        allOutput = s.ReadToEnd();
                    }
                    else
                    {
                        var json = new NotificationExceptionJSONObject(new Exception(string.Format("HTTP Status: {0} - {1}", response.StatusCode, response.StatusDescription)), Erro.Severidade.Error).ToJSON();

                        allOutput += json;
                    }
                }
            }
            catch (WebException e)
            {
                allOutput += new NotificationEndJSONObject(e.Status.ToString()).ToJSON();
            }

            return allOutput;
        }

        /// <summary>
        /// The Run
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        private string Run()
        {
            switch (TipoAplicacao.ID)
            {
                case 1: //EXE
                    return RunWithOutput();

                case 2: //WEB
                    //Suportar autenticação
                    return CallWebService();

                default:
                    return new NotificationExceptionJSONObject(new Exception(string.Format("Implementar o tipo de aplicação {0} - {1}", TipoAplicacao.ID, TipoAplicacao.Nome)), Erro.Severidade.Critical).ToJSON();
            }
        }

        /// <summary>
        /// The CopyFrom
        /// </summary>
        /// <param name="a">The a<see cref="Aplicacao"/></param>
        private void CopyFrom(Aplicacao a)
        {
            this.ID = a.ID;
            this.Nome = a.Nome;
            this.Descricao = a.Descricao;
            this.IntervaloTempo = a.IntervaloTempo;
            this.Caminho = a.Caminho;
            this.EstadoExecucao = a.EstadoExecucao;
            this.TipoAplicacao = a.TipoAplicacao;
            this.Args = a.Args;
        }

        /// <summary>
        /// The Execute
        /// </summary>
        /// <param name="context">The context<see cref="IJobExecutionContext"/></param>
        public void Execute(IJobExecutionContext context)
        {
            Thread t = new Thread(() =>
            {
                try
                {
                    var thisAplicacao = (Aplicacao)context.MergedJobDataMap["Aplicacao"];

                    thisAplicacao = DBMaestro.Aplicacoes.Aplicacao_S(thisAplicacao.ID);

                    CopyFrom(thisAplicacao);

                    if (thisAplicacao != null && thisAplicacao.Ativo)
                    {
                        //POR AGORA FAZ-SE ASSIM E ACABOU
                        //Gerir estados de execução
                        switch (thisAplicacao.EstadoExecucao)
                        {
                            case (int)MaestroStuff.Enums.EstadoExecucao.Erro:
                            case (int)MaestroStuff.Enums.EstadoExecucao.Disponivel:

                                thisAplicacao.EstadoExecucao = (int)MaestroStuff.Enums.EstadoExecucao.EmExecucao;
                                thisAplicacao.IDLog = DBMaestro.Aplicacoes.Aplicacao_Estado_U(thisAplicacao);

                                //thisAplicacao.IDLog = DBMaestro.Aplicacoes.Aplicacao_Log_GetNewID();

                                var sw = new Stopwatch();
                                sw.Start();

                                string output = Run();

                                var nots = output.Split(new char[] { '\n', '\r' }).Select(x => Notification.FromJSON(x)).ToList();


                                if (nots.Where(x => x is NotificationStartJSONObject).Count() == 0)
                                {
                                    switch (thisAplicacao.TipoAplicacao.ID)
                                    {
                                        case 1:
                                            nots.Insert(0, new NotificationStartJSONObject(string.Format("A executar a aplicação {0} - {1}", Nome, Caminho)));
                                            break;

                                        case 2:
                                            nots.Insert(0, new NotificationStartJSONObject(string.Format("A chamar o web service {0} - {1}", Nome, Caminho)));
                                            break;
                                    }
                                }

                                if (nots.Where(x => x is NotificationEndJSONObject).Count() == 0)
                                {
                                    switch (thisAplicacao.TipoAplicacao.ID)
                                    {
                                        case 1:
                                            nots.Insert(nots.Count, new NotificationEndJSONObject(string.Format("Fim da execução da aplicação {0} - {1}", Nome, Caminho)));
                                            break;

                                        case 2:
                                            nots.Insert(nots.Count, new NotificationEndJSONObject(string.Format("Fim da chamda ao web service {0} - {1}", Nome, Caminho)));
                                            break;
                                    }
                                }


                                var reader = new StringReader(output);
                                var line = reader.ReadLine();

                                while (!string.IsNullOrWhiteSpace(line))
                                {
                                    Notification n = Notification.FromJSON(line);

                                    if (n == null)
                                    {
                                        n = new NotificationInfoJSONObject(line);
                                    }

                                    if (n is NotificationExceptionJSONObject)
                                    {
                                        var nEx = (NotificationExceptionJSONObject)NotificationExceptionJSONObject.FromJSON(line);

                                        while (nEx != null)
                                        {
                                            DBMaestro.Aplicacoes.Aplicacao_Log_I(thisAplicacao, nEx);
                                            nEx = nEx.InnerException;
                                        }

                                        thisAplicacao.EstadoExecucao = (int)MaestroStuff.Enums.EstadoExecucao.Erro;
                                        DBMaestro.Aplicacoes.Aplicacao_Estado_U(thisAplicacao);
                                    }
                                    else if (n is NotificationInfoJSONObject)
                                    {
                                        var nInfo = (NotificationInfoJSONObject)NotificationInfoJSONObject.FromJSON(line);

                                        DBMaestro.Aplicacoes.Aplicacao_Log_I(thisAplicacao, nInfo);
                                    }
                                    else if (n is NotificationStartJSONObject)
                                    {
                                        var nStart = (NotificationStartJSONObject)NotificationStartJSONObject.FromJSON(line);

                                        DBMaestro.Aplicacoes.Aplicacao_Log_I(thisAplicacao, nStart);
                                    }
                                    else if (n is NotificationEndJSONObject)
                                    {
                                        var nEnd = (NotificationEndJSONObject)NotificationEndJSONObject.FromJSON(line);

                                        DBMaestro.Aplicacoes.Aplicacao_Log_I(thisAplicacao, nEnd);

                                        thisAplicacao.EstadoExecucao = (int)MaestroStuff.Enums.EstadoExecucao.Disponivel;
                                        DBMaestro.Aplicacoes.Aplicacao_Estado_U(thisAplicacao);
                                    }
                                    else if (n != null)
                                    {
                                    }

                                    line = reader.ReadLine();
                                }

                                sw.Stop();
                                thisAplicacao.DuracaoUltimaExecucao = sw.Elapsed;

                                thisAplicacao.ProximaExecucao = context.NextFireTimeUtc != null ? context.NextFireTimeUtc.Value.LocalDateTime : (DateTime?)null;
                                thisAplicacao.UltimaExecucao = DateTime.Now;

                                DBMaestro.Aplicacoes.Aplicacao_DatasExecucao_U(thisAplicacao.ID, thisAplicacao.UltimaExecucao, thisAplicacao.ProximaExecucao, thisAplicacao.DuracaoUltimaExecucao);
                                break;

                            case (int)MaestroStuff.Enums.EstadoExecucao.EmExecucao:
                                var exExec = new Exception(string.Format("A aplicação {0} encontra-se com o estado 2 - Em execução, e não será iniciada.", thisAplicacao.Nome));
                                LogHandling.LogErrorEmail(exExec, Erro.Severidade.Info);

                                return;
                            //case (int)MaestroStuff.Enums.EstadoExecucao.Erro:
                            //    var exErro = new Exception(string.Format("A aplicação {0} encontra-se com o estado 3 - Erro, e não será iniciada.", thisAplicacao.Nome));
                            //    ErrorHandling.LogErrorEmail(exErro, Erro.Severidade.Error);

                            //    return;
                            case (int)MaestroStuff.Enums.EstadoExecucao.ErroCritico:
                                var exCritico = new Exception(string.Format("A aplicação {0} encontra-se com o estado 4 - Erro crítico, e não será iniciada.", thisAplicacao.Nome));
                                LogHandling.LogErrorEmail(exCritico, Erro.Severidade.Critical);

                                return;

                            case (int)MaestroStuff.Enums.EstadoExecucao.Parado:
                                var exParada = new Exception(string.Format("A aplicação {0} encontra-se com o estado 5 - Parada, e não será iniciada.", thisAplicacao.Nome));
                                LogHandling.LogErrorEmail(exParada, Erro.Severidade.Info);

                                return;

                            default:
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    var a = (Aplicacao)context.MergedJobDataMap["Aplicacao"];
                    var name = "";

                    if (a == null) name = "Não foi possível obter a aplicação";
                    else
                    {
                        name = a.Nome;
                        a.EstadoExecucao = (int)MaestroStuff.Enums.EstadoExecucao.Disponivel;
                        DBMaestro.Aplicacoes.Aplicacao_Estado_U(a);
                    }

                    var ex2 = new Exception(string.Format("[{0} (id: {1})] - {2}: {3}", name, a.ID, ex.GetType().ToString(), ex.Message), ex);
                    LogHandling.LogErrorEmail(ex2, Erro.Severidade.Critical);
                }
            });

            t.Start();
        }

        /// <summary>
        /// Defines the <see cref="Tipo" />
        /// </summary>
        public class Tipo
        {
            /// <summary>
            /// Gets or sets the ID
            /// </summary>
            public int ID { get; set; }

            /// <summary>
            /// Gets or sets the Nome
            /// </summary>
            public string Nome { get; set; }

            /// <summary>
            /// Gets or sets the Descricao
            /// </summary>
            public string Descricao { get; set; }
        }
    }
}
