﻿using MaestroService.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using MaestroStuff.Tools;
using System.Net;
using System.Net.Sockets;

namespace MaestroService.Tools
{
    public static class Toolkit
    {
        public static class Colisoes
        {
            public static string MakeUniqueName(string name, IEnumerable<string> allNames)
            {
                string newName = name;

                for (int i = 1; ; ++i)
                {
                    if (allNames.Where(x => x == newName).Count() == 0)
                        return newName;

                    newName = string.Format("{0}_{1}", name, i);
                }
            }
            public static string MakeUniqueFileName(string name, IEnumerable<string> allNames)
            {
                string newName = name;

                for (int i = 1; ; ++i)
                {
                    if (allNames.Where(x => x == newName).Count() == 0)
                        return newName;

                    newName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(name), i, Path.GetExtension(name));
                }
            }
        }

        public static class Email
        {
            public static bool SendEmail(string from, List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                if (to == null || to.Count == 0) return false;

                try
                {
                    MailMessage mail = new MailMessage(from, to.First());
                    foreach (var toAddress in to.Where(x => x != to.First()))
                    {
                        mail.To.Add(toAddress);
                    }
                    foreach (var ccAddress in cc ?? new List<string>())
                    {
                        mail.CC.Add(ccAddress);
                    }
                    foreach (var bccAddress in bcc ?? new List<string>())
                    {
                        mail.Bcc.Add(bccAddress);
                    }
                    SmtpClient client = new SmtpClient();
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Host = AppConfigKeys.Configuration.Erros.Email.SMTPServer;
                    mail.Subject = subject;
                    mail.Body = body;
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
            public static bool SendEmail(List<string> to, string subject, string body, List<string> cc = null, List<string> bcc = null)
            {
                return SendEmail(AppConfigKeys.Configuration.Erros.Email.From, to, subject, body, cc, bcc);
            }
        }

        public static class Enderecos
        {
            public static string LocalHostName
            {
                get
                {
                    return Dns.GetHostName();
                }
            }
            public static List<string> LocalIP
            {
                get
                {
                    var res = new List<string>();
                    var host = Dns.GetHostEntry(LocalHostName);

                    foreach (var ip in host.AddressList)
                    {
                        if (ip.AddressFamily == AddressFamily.InterNetwork)
                        {
                            res.Add(ip.ToString());
                        }
                    }

                    return res;
                }
            }

            public static string ARPResolveMAC(string mac)
            {
                var arpStream = ExecuteCommandLine("arp", "-a");

                // Consume first three lines
                for (int i = 0; i < 3; i++)
                {
                    arpStream.ReadLine();
                }

                // Read entries 

                while (!arpStream.EndOfStream)
                {
                    var line = arpStream.ReadLine().Trim();

                    var parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries); // Match device's MAC address 

                    if (parts[1].Replace("-", "").ToUpper().Trim() == mac.Replace(new string[] { ":", "-", " " }, ""))
                    { // Your device is on the network // Access its IP address with: parts[0].Trim() 
                        return parts[0].Trim();
                    }
                }

                return "";
            }
        }

        private static StreamReader ExecuteCommandLine(String file, String arguments = "")
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = file;
            startInfo.Arguments = arguments;

            Process process = Process.Start(startInfo);

            return process.StandardOutput;
        }
    }
}
