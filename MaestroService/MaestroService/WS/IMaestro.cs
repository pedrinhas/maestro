﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using MaestroService.WS;

namespace MaestroService.WS
{
    [ServiceContract]
    public interface IMaestro
    {
        void InitializeService();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ForceApplicationStart?id={id}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool ForceApplicationStart(int id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllJobs", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<MaestroWCFService.WSAplicacao> GetAllJobs();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetActiveJobs", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<MaestroWCFService.WSAplicacao> GetActiveJobs();
    }
}
