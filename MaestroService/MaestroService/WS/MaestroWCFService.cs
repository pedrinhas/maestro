﻿using MaestroService.Config;
using MaestroService.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace MaestroService.WS
{
    public class MaestroWCFService : IMaestro
    {
        #region Initialization

        WebServiceHost host = null;
        ServiceEndpoint endpoint = null;

        public void InitializeService()
        {
            try
            {
                host = new WebServiceHost(typeof(MaestroWCFService), new Uri(string.Format("http://localhost:{0}/IMaestro/", AppConfigKeys.Configuration.WS.Port)));
                endpoint = host.AddServiceEndpoint(typeof(IMaestro), new WebHttpBinding(), "");

                var webHttp = new WebHttpBehavior()
                {
                    HelpEnabled = true
                };

                endpoint.EndpointBehaviors.Add(webHttp);

                host.Open();

            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }

        public void CloseService()
        {
            try
            {

                if (host != null)
                {
                    host.Close();
                    host = null;
                    endpoint = null;
                };
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }

        #endregion

        public bool ForceApplicationStart(int id)
        {
            try
            {
                var res = MaestroService.ForceStartApplication(id);

                if (res)
                {
                    var forced = GetActiveJobs().SingleOrDefault(x => x.ID == id);

                    LogHandling.LogInfo(AppConfigKeys.Configuration.Erros.Email.To, string.Format("Force start {0}", forced.Nome), string.Format("Foi forçada a execução da aplicação {0}: {1}", forced.ID, forced.Nome), MaestroStuff.Enums.Erro.Severidade.Info);
                }

                return res;
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                return false;
            }
        }

        public List<WSAplicacao> GetAllJobs()
        {
            try
            {
                return MaestroService.GetAllJobs();
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                return null;
            }
        }

        public List<WSAplicacao> GetActiveJobs()
        {
            try
            {
                return MaestroService.GetActiveJobs();
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
                return null;
            }
        }




        public class WSAplicacao
        {
            public int ID { get; set; }
            public string Nome { get; set; }
            public bool Ativo { get; set; }
        }
    }
}
