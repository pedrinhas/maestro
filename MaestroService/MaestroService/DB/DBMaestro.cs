﻿using MaestroService.Aplicacoes;
using MaestroService.Config;
using MaestroStuff.Notifications;
using MaestroStuff.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaestroService.DB
{
    public static class DBMaestro
    {
        #region Aplicacoes
        public static class Aplicacoes
        {
            public static List<Aplicacao> Aplicacao_LS()
            {
                //return new List<Aplicacao>();

                try
                {
                    var res = new List<Aplicacao>();

                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_Aplicacao_LS", con))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            if(con.State != System.Data.ConnectionState.Open) con.Open();

                            var reader = cmd.ExecuteReader();

                            var tipos = new List<Aplicacao.Tipo>();

                            while (reader.Read())
                            {
                                var a = new Aplicacao()
                                {
                                    ID = Convert.ToInt32(reader[0]),
                                    Nome = reader[4].ToString(),
                                    Descricao = reader[5].ToString(),
                                    Caminho = reader[6].ToString(),
                                    IntervaloTempo = reader[7].ToString(),
                                    EstadoExecucao = Convert.ToInt32(reader[8]),
                                    Ativo = Convert.ToBoolean(reader[12])
                                };

                                int tmpIDTipo = Convert.ToInt32(reader[1]);
                                Aplicacao.Tipo t = new Aplicacao.Tipo();
                                if (tipos.Where(x => x.ID == tmpIDTipo).Count() == 0)
                                {
                                    t.ID = tmpIDTipo;
                                    t.Nome = reader[2].ToString();
                                    t.Descricao = reader[3].ToString();

                                    tipos.Add(t);
                                }
                                else
                                {
                                    t = tipos.Where(x => x.ID == tmpIDTipo).First();
                                }
                                a.TipoAplicacao = t;

                                if (reader[9] != null)
                                {
                                    var date = DateTime.MinValue;

                                    if (DateTime.TryParse(reader[9].ToString(), out date))
                                    {
                                        a.UltimaExecucao = date;
                                    }
                                    else
                                    {
                                        a.UltimaExecucao = DateTime.MinValue;
                                    }
                                }
                                if (reader[10] != null)
                                {
                                    var date = DateTime.MaxValue;

                                    if (DateTime.TryParse(reader[10].ToString(), out date))
                                    {
                                        a.ProximaExecucao = date;
                                    }
                                    else
                                    {
                                        a.ProximaExecucao = DateTime.MaxValue;
                                    }
                                }

                                res.Add(a);
                            }

                            reader.Close();
                        }

                        using (SqlCommand cmd = new SqlCommand("sp_Aplicacao_Parametros_LS", con))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            if (con.State != System.Data.ConnectionState.Open) con.Open();

                            var reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                var idApp = reader.GetInt32("idAplicacao");
                                var value = reader.GetString("val");

                                res.Single(x => x.ID == idApp).Args.Add(value);
                            }
                        }

                        return res;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static Aplicacao Aplicacao_S(int id)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("sp_Aplicacao_S", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                        con.Open();

                        var reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var a = new Aplicacao();

                            a.ID = Convert.ToInt32(reader[0]);
                            a.Nome = reader[1].ToString();
                            a.Descricao = reader[2].ToString();
                            a.Caminho = reader[3].ToString();
                            a.IntervaloTempo = reader[4].ToString();
                            a.EstadoExecucao = Convert.ToInt32(reader[5]);

                            a.Ativo = Convert.ToBoolean(reader[9]);

                            a.TipoAplicacao = new Aplicacao.Tipo();
                            a.TipoAplicacao.ID = Convert.ToInt32(reader[10]);
                            a.TipoAplicacao.Nome = reader[11].ToString();
                            a.TipoAplicacao.Descricao = reader[12].ToString();

                            reader.Close();


                            //um cibo esparguete, mas funciona e não é assim tão confuso
                            using (SqlCommand cmd2 = new SqlCommand("[sp_Aplicacao_Parametros_S]", con))
                            {
                                cmd2.CommandType = System.Data.CommandType.StoredProcedure;

                                cmd2.Parameters.Add("@idAplicacao", System.Data.SqlDbType.Int).Value = a.ID;

                                var reader2 = cmd2.ExecuteReader();

                                while (reader2.Read())
                                {
                                    var idApp = reader2.GetInt32("idAplicacao");
                                    var value = reader2.GetString("val");

                                    a.Args.Add(value);
                                }
                            }

                            return a;
                        }
                    }

                    //SOMETHING WENT WRONG
                    return null;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public static int Aplicacao_I(Aplicacao app)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("sp_Aplicacao_S", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = app.ID;
                        cmd.Parameters.Add("@nome", System.Data.SqlDbType.NVarChar).Value = app.Nome;
                        cmd.Parameters.Add("@descricao", System.Data.SqlDbType.NVarChar).Value = app.Descricao;
                        cmd.Parameters.Add("@caminhoExe", System.Data.SqlDbType.NVarChar).Value = app.Caminho;
                        cmd.Parameters.Add("@intervaloTempo", System.Data.SqlDbType.NVarChar).Value = app.IntervaloTempo;

                        con.Open();

                        var res = cmd.ExecuteScalar();
                        return Convert.ToInt32(res);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            public static void Aplicacao_DatasExecucao_U(int id, DateTime ultimaExec, DateTime? proximaExec, TimeSpan duracaoUltimaExec)
            {
                try
                {
                    using(SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("[sp_Aplicacao_DatasExecucao_U]", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@idAplicacao", System.Data.SqlDbType.Int).Value = id;
                        cmd.Parameters.Add("@ultimaExec", System.Data.SqlDbType.DateTime).Value = ultimaExec;
                        cmd.Parameters.Add("@proximaExec", System.Data.SqlDbType.DateTime).Value = proximaExec;
                        cmd.Parameters.Add("@duracaoExec", System.Data.SqlDbType.Time).Value = duracaoUltimaExec;

                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            public static void Aplicacao_DatasExecucao_U(Aplicacao app)
            {
                Aplicacao_DatasExecucao_U(app.ID, app.UltimaExecucao, app.ProximaExecucao, app.DuracaoUltimaExecucao);
            }

            public static int Aplicacao_Estado_U(int idAplicacao, int idEstado, string comentario)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("[sp_Aplicacao_Estado_U]", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@idAplicacao", System.Data.SqlDbType.Int).Value = idAplicacao;
                        cmd.Parameters.Add("@idEstado", System.Data.SqlDbType.Int).Value = idEstado;
                        cmd.Parameters.Add("@comentario", System.Data.SqlDbType.NVarChar).Value = comentario;

                        con.Open();

                        var res = cmd.ExecuteScalar();
                        return Convert.ToInt32(res);
                        
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            public static int Aplicacao_Estado_U(Aplicacao app)
            {
                return app.IDLog = Aplicacao_Estado_U(app.ID, app.EstadoExecucao, string.Format("Estado alterado para {0} - {1}", app.EstadoExecucao, (MaestroStuff.Enums.EstadoExecucao)app.EstadoExecucao));
            }


            public static int Aplicacao_Log_GetNewID()
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("[sp_Aplicacao_Log_GetNewID]", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        con.Open();

                        var res = Convert.ToInt32(cmd.ExecuteScalar());

                        return res;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            public static void Aplicacao_Log_I(int idAplicacao, int idLog, string data, MaestroStuff.Enums.Erro.Severidade severidade, string texto, string exClass, string exStackTrace, string exTargetSite, string exMessage)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(AppConfigKeys.ConnectionStrings.Maestro))
                    {
                        SqlCommand cmd = new SqlCommand("[sp_Aplicacao_Log_I]", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@idLog", System.Data.SqlDbType.Int).Value = idLog;
                        cmd.Parameters.Add("@data", System.Data.SqlDbType.DateTime).Value = data;
                        cmd.Parameters.Add("@severidade", System.Data.SqlDbType.Int).Value = (int)severidade;
                        cmd.Parameters.Add("@texto", System.Data.SqlDbType.NVarChar).Value = texto ?? "";
                        cmd.Parameters.Add("@exClass", System.Data.SqlDbType.NVarChar).Value = exClass ?? "";
                        cmd.Parameters.Add("@exStackTrace", System.Data.SqlDbType.NVarChar).Value = exStackTrace ?? "";
                        cmd.Parameters.Add("@exTargetSite", System.Data.SqlDbType.NVarChar).Value = exTargetSite ?? "";
                        cmd.Parameters.Add("@exMessage", System.Data.SqlDbType.NVarChar).Value = exMessage ?? "";

                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            public static void Aplicacao_Log_I(Aplicacao app, Notification not)
            {
                if (not is NotificationExceptionJSONObject)
                {
                    var nEx = (NotificationExceptionJSONObject)not;
                    Aplicacao_Log_I(app.ID, app.IDLog, not.Data, nEx.Severidade, nEx.Texto, nEx.Class, nEx.StackTrace, nEx.TargetSite, nEx.Message);
                }
                else if (not is NotificationInfoJSONObject)
                {
                    var nInfo = (NotificationInfoJSONObject)not;
                    Aplicacao_Log_I(app.ID, app.IDLog, not.Data, MaestroStuff.Enums.Erro.Severidade.Info, nInfo.Texto, null, null, null, null);
                }
                else if (not is NotificationStartJSONObject)
                {
                    var nStart = (NotificationStartJSONObject)not;
                    Aplicacao_Log_I(app.ID, app.IDLog, not.Data, MaestroStuff.Enums.Erro.Severidade.Info, nStart.Texto, null, null, null, null);
                } if (not is NotificationEndJSONObject)
                {
                    var nEnd = (NotificationEndJSONObject)not;
                    Aplicacao_Log_I(app.ID, app.IDLog, not.Data, MaestroStuff.Enums.Erro.Severidade.Info, nEnd.Texto, null, null, null, null);
                }
            }
        }
        #endregion
    }
}
