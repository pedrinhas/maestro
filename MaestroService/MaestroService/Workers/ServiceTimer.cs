﻿using System;
using System.Configuration;
using System.IO;
using System.Timers;
using System.Collections.Generic;
using MaestroService.Config;
using Quartz;
using Quartz.Impl;

namespace MaestroService.Workers
{
    public static class ServiceTimer
    {
        private static ISchedulerFactory schedFactory = new StdSchedulerFactory();
        private static IScheduler sched = schedFactory.GetScheduler();

        public static void Initialize()
        {
            RegisterJob();

            MaestroWorker.DoWork();
        }

        public static void RegisterJob()
        {
            IJobDetail mainJob = JobBuilder.Create<MaestroWorker>().WithIdentity("Main job", "Main").WithDescription("Job principal do Maestro. Esta job corre uma vez por dia, todos os dias. Serve para ler as configurações da BD").Build();

            mainJob.JobDataMap.Add("Maestro", "Main job");

            ITrigger mainTrig = TriggerBuilder.Create().WithIdentity("Main job").WithDescription("Job principal do Maestro. Esta job corre uma vez por dia, todos os dias. Serve para ler as configurações da BD").WithCronSchedule(string.Format("30 {0} {1} * * ?", AppConfigKeys.Configuration.Timer.Minutes, AppConfigKeys.Configuration.Timer.Hours), x => x.WithMisfireHandlingInstructionFireAndProceed()).ForJob(mainJob).Build();

            sched.ScheduleJob(mainJob, mainTrig);

            sched.Start();
        }

        public static void Dispose()
        {
            schedFactory = null;
            sched.Shutdown();
            sched = null;
        }
    }
}
