﻿using MaestroService.Aplicacoes;
using MaestroService.DB;
using MaestroService.Tools;
using NCrontab.Advanced;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaestroService.WS;

namespace MaestroService.Workers
{
    public class MaestroWorker : IJob
    {
        private static ISchedulerFactory schedFactory = new StdSchedulerFactory();
        private static IScheduler sched = schedFactory.GetScheduler();

        #region Control

        public static bool TriggerJob(int id)
        {
            foreach (var job in sched.GetJobKeys(Quartz.Impl.Matchers.GroupMatcher<JobKey>.GroupEquals("Aplicacoes")))
            {
                var detail = sched.GetJobDetail(job);
                if(detail.JobDataMap["Aplicacao"] is Aplicacao)
                {
                    var app = detail.JobDataMap["Aplicacao"] as Aplicacao;

                    if (app.ID == id && app.Ativo)
                    {
                        sched.TriggerJob(job);

                        return true;
                    }
                }
            }
            return false;
        }

        public static List<MaestroWCFService.WSAplicacao> GetAllJobs()
        {
            var allKeys = sched.GetJobKeys(Quartz.Impl.Matchers.GroupMatcher<JobKey>.GroupEquals("Aplicacoes"));
            var res = new List<MaestroWCFService.WSAplicacao>();

            foreach (var k in allKeys)
            {
                var details = sched.GetJobDetail(k);
                var app = (Aplicacao)details.JobDataMap["Aplicacao"];

                if (app != null)
                {
                    res.Add(new MaestroWCFService.WSAplicacao() { ID = app.ID, Nome = app.Nome, Ativo = app.Ativo });
                }
            }

            return res;
        }

        public static List<MaestroWCFService.WSAplicacao> GetActiveJobs()
        {
            return GetAllJobs().Where(x => x.Ativo).ToList(); 
        }

        #endregion
        
        public static void DoWork()
        {
            var jobKeys = sched.GetJobKeys(Quartz.Impl.Matchers.GroupMatcher<JobKey>.GroupEquals("Aplicacoes")).ToList();
            sched.DeleteJobs(jobKeys);

            var lst = DBMaestro.Aplicacoes.Aplicacao_LS();

            var allJobNames = new List<string>();

            foreach (var item in lst)
            {
                var n = Toolkit.Colisoes.MakeUniqueName(item.Nome, allJobNames);

                IJobDetail job = JobBuilder.Create<Aplicacao>().WithIdentity(n, "Aplicacoes").WithDescription(item.Descricao).Build();

                //Adiciona o context para saber que aplicação se executa
                job.JobDataMap.Add("Aplicacao", item);

                ITrigger trig = TriggerBuilder.Create().WithIdentity(n).WithDescription(item.Descricao).WithCronSchedule(item.IntervaloTempo, x => x.WithMisfireHandlingInstructionFireAndProceed()).ForJob(job).Build();

                sched.ScheduleJob(job, trig);

                allJobNames.Add(n);
            }

            sched.Start();
        }

        public void Execute(IJobExecutionContext context)
        {
            MaestroWorker.DoWork();
        }
    }
}
