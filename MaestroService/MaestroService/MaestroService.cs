﻿using MaestroService.Config;
using MaestroService.Log;
using MaestroService.Workers;
using MaestroService.WS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MaestroService.WS;

namespace MaestroService
{
    public partial class MaestroService : ServiceBase
    {
        #region Singleton
        private MaestroService()
        {
            InitializeComponent();
        }

        private static MaestroService thisInstance;
        public static MaestroService GetInstance()
        {
            return (thisInstance ?? (thisInstance = new MaestroService()));
        }

        #endregion

        #region Control

        public static bool ForceStartApplication(int id)
        {
            return MaestroWorker.TriggerJob(id);
        }

        public static List<MaestroWCFService.WSAplicacao> GetAllJobs()
        {
            return MaestroWorker.GetAllJobs();
        }
        public static List<MaestroWCFService.WSAplicacao> GetActiveJobs()
        {
            return MaestroWorker.GetActiveJobs();
        }

        #endregion

        #region WS

        private MaestroWCFService ws = new MaestroWCFService();

#if SERVICE
#if DEBUG
#endif
#else
        private void startWS()
        {
            ws.InitializeService();
        }
#endif
        #endregion

        private static Aplicacoes.Aplicacao app;
        private static Aplicacoes.Aplicacao MainAplicacao
        {
            get
            {
                if (app == null)
                {
                    app = new Aplicacoes.Aplicacao()
                        {
                            ID = 0,
                            Nome = "Maestro Principal"
                        };
                    app.IDLog = DB.DBMaestro.Aplicacoes.Aplicacao_Log_GetNewID();
                }

                return app;
            }
        }

        private static string LogPath { get { return string.Format("{0}\\{1}", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "log.txt"); } }

        static void Main(string[] args)
        {
            try
            {
#if SERVICE
#if DEBUG
                System.Diagnostics.Debugger.Launch();
#endif
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { MaestroService.GetInstance() };
                ServiceBase.Run(ServicesToRun);
#else
                WriteToFile(LogPath, "Maestro Main");

                MaestroService service = MaestroService.GetInstance();
                service.StartDebug(args);

                LogHandling.LogInfo(AppConfigKeys.Configuration.Erros.Email.To, "Maestro start", "");

                service.startWS();
#endif
            }
            catch (Exception ex)
            {
                var no = new MaestroStuff.Notifications.NotificationExceptionJSONObject(ex, MaestroStuff.Enums.Erro.Severidade.Critical);
                DB.DBMaestro.Aplicacoes.Aplicacao_Log_I(MainAplicacao, no);

                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }
        protected override void OnStop()
        {
            try
            {
                DB.DBMaestro.Aplicacoes.Aplicacao_Log_I(MainAplicacao, new MaestroStuff.Notifications.NotificationInfoJSONObject()
                {
                    Texto = "Maestro stop",
                    Tipo = MaestroStuff.Notifications.NotificationType.Info
                });

                if (ws != null)
                {
                    ws.CloseService();
                    ws = null;
                }

                LogHandling.LogInfo(AppConfigKeys.Configuration.Erros.Email.To, "Maestro stop", "", MaestroStuff.Enums.Erro.Severidade.Warning);

                WriteToFile(LogPath, "Maestro stop");

                ServiceTimer.Dispose();
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                DB.DBMaestro.Aplicacoes.Aplicacao_Log_I(MainAplicacao, new MaestroStuff.Notifications.NotificationInfoJSONObject()
                {
                    Texto = "Maestro start",
                    Tipo = MaestroStuff.Notifications.NotificationType.Info
                });

                ws.InitializeService();

                WriteToFile(LogPath, "Maestro start");

                LogHandling.LogInfo(AppConfigKeys.Configuration.Erros.Email.To, "Maestro start", "");

                ServiceTimer.Initialize();
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }

        private void StartDebug(string[] args)
        {
            try
            {

                DB.DBMaestro.Aplicacoes.Aplicacao_Log_I(MainAplicacao, new MaestroStuff.Notifications.NotificationInfoJSONObject()
                {
                    Texto = "Maestro start",
                    Tipo = MaestroStuff.Notifications.NotificationType.Info
                });
                ServiceTimer.Initialize();
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }


        private static void WriteToFile(string path, string text)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                using (var writer = new StreamWriter(fs))
                {
                    writer.WriteLine(text);
                }
            }
            catch (Exception ex)
            {
                LogHandling.LogError(ex, MaestroStuff.Enums.Erro.Severidade.Error);
            }
        }
    }
}
