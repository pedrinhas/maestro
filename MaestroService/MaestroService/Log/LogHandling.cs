﻿using MaestroService.Tools;
using MaestroService.Config;
using MaestroStuff.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MaestroService.Log
{
    public static class LogHandling
    {
        private static Erro.Severidade MinimumSeverity
        {
            get
            {
                int level = AppConfigKeys.Configuration.Erros.LogLevel;

                if (level > (int)Erro.Severidade.Critical)
                    return Erro.Severidade.Critical;
                else if (level < (int)Erro.Severidade.Info)
                    return Erro.Severidade.Info;
                else return (Erro.Severidade)level;
            }
        }

        public static bool LogError(Exception ex, Erro.Severidade severity = Erro.Severidade.Error)
        {
            if (severity < MinimumSeverity) return false;

            bool email = false, db = false;

            if (AppConfigKeys.Configuration.Erros.Email.Log) email = LogErrorEmail(ex, severity);
            db = LogErrorDB(ex, severity);

            return email && db;
        }
        public static bool LogErrorEmail(List<string> to, string subject, string body, Exception ex, Erro.Severidade severity = Erro.Severidade.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < MinimumSeverity) return false;

            if (!string.IsNullOrWhiteSpace(body)) body = body.TrimEnd(new char[] { '\n', '\r' }) + string.Format("{0}{0}", Environment.NewLine);
            body += string.Format("Hostname:{0}{1}{2}{0}{0}", Environment.NewLine, "\t", Toolkit.Enderecos.LocalHostName);
            body += string.Format("Local IP:{0}{1}{2}{0}{0}", Environment.NewLine, "\t", string.Join(string.Format("{0}{1}", Environment.NewLine, "\t"), Toolkit.Enderecos.LocalIP));

            body = AddHttpContextToBody(body);

            string exText = "";

            do
            {
                exText += string.Format("{0} - {1}", ex.GetType().ToString(), ex.Message);

                if (!string.IsNullOrWhiteSpace(ex.StackTrace)) exText += string.Format("{0}{1}StackTrace:{0}{1}{2}", Environment.NewLine, "\t", ex.StackTrace);
                if (ex.TargetSite != null && !string.IsNullOrWhiteSpace(ex.TargetSite.ToString())) exText += string.Format("{0}{0}{1}TargetSite:{0}{1}{2}{3}{3}", Environment.NewLine, "\t", ex.TargetSite, ex.InnerException == null ? "" : Environment.NewLine);

                ex = ex.InnerException;
            } while (ex != null);

            var bodyAll = "";

            if (string.IsNullOrWhiteSpace(body))
            {
                bodyAll = string.Format("Exceptions{0}{1}{2}", Environment.NewLine, "\t", exText);
            }
            else
            {
                bodyAll = string.Format("{2}{0}{0}Exceptions{0}{1}{3}", Environment.NewLine, "\t", body.TrimEnd(new char[] { '\n', '\r' }), exText);
            }

            return Toolkit.Email.SendEmail(to, subject, bodyAll, cc, bcc);
        }

        public static bool LogErrorEmail(List<string> to, Exception ex, Erro.Severidade severity = Erro.Severidade.Error, List<string> cc = null, List<string> bcc = null)
        {
            if (severity < MinimumSeverity) return false;

            string subject = string.Format("[{2}] [Maestro] Exception: {0} - {1}", ex.GetType().ToString(), ex.Message, severity.ToString());

            return LogErrorEmail(to, subject, "", ex, severity, cc, bcc);
        }

        public static bool LogErrorEmail(Exception ex, Erro.Severidade severity = Erro.Severidade.Error)
        {
            if (severity < MinimumSeverity) return false;

            return LogErrorEmail(AppConfigKeys.Configuration.Erros.Email.To, ex, severity, AppConfigKeys.Configuration.Erros.Email.CC, AppConfigKeys.Configuration.Erros.Email.BCC);
        }
        public static bool LogErrorDB(Exception ex, Erro.Severidade severity = Erro.Severidade.Error)
        {
            if (severity < MinimumSeverity) return false;

            return true;
        }

        public static bool LogInfo(List<string> to, string subject, string body, Erro.Severidade severity = Erro.Severidade.Info, List<string> cc = null, List<string> bcc = null)
        {
            if (severity >  Erro.Severidade.Warning)
            {
                return LogErrorEmail(to, subject, body, new Exception(subject), severity, cc, bcc);
            }

            if (!string.IsNullOrWhiteSpace(body)) body = body.TrimEnd(new char[] { '\n', '\r' }) + string.Format("{0}{0}", Environment.NewLine);
            body += string.Format("Hostname:{0}{1}{2}{0}{0}", Environment.NewLine, "\t", Toolkit.Enderecos.LocalHostName);
            body += string.Format("Local IP:{0}{1}{2}{0}{0}", Environment.NewLine, "\t", string.Join(string.Format("{0}{1}", Environment.NewLine, "\t"), Toolkit.Enderecos.LocalIP));

            body = AddHttpContextToBody(body);

            subject = string.Format("[{0}] [Maestro] Info: {1}", severity.ToString(), subject);

            return Toolkit.Email.SendEmail(to, subject, body, cc, bcc);
        }

        private static string AddHttpContextToBody(string body)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Response != null)
            {
                body += string.Format("IP{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.UserHostAddress);
                body += string.Format("URL{0}{1}{2}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Request.Url);
                body += string.Format("Http Status Code{0}{1}{2} - {3}{0}{0}", Environment.NewLine, "\t", HttpContext.Current.Response.StatusCode + "." + HttpContext.Current.Response.SubStatusCode, HttpContext.Current.Response.StatusDescription);

                body += string.Format("Request Headers{0}", Environment.NewLine);

                foreach (var header in HttpContext.Current.Request.Headers.AllKeys)
                {
                    var val = HttpContext.Current.Request.Headers[header];

                    body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
                }

                body += Environment.NewLine;

                body += string.Format("Response Headers{0}", Environment.NewLine);

                foreach (var header in HttpContext.Current.Response.Headers.AllKeys)
                {
                    var val = HttpContext.Current.Response.Headers[header];

                    body += string.Format("{1}{2}: {3}{0}", Environment.NewLine, "\t", header, val);
                }
            }

            return body;
        }
    }
}