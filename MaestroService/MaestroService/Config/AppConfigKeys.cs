﻿using MaestroService.Log;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaestroService.Config
{
    public static class AppConfigKeys
    {
        private static string getValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        public static bool Debug { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Debug") ?? "False").ToLowerInvariant()); } }
        public static class WebServices
        {
            //public static class SIDE
            //{
            //    public static string Endpoint { get { return getValue("WebServices.SIDE.Endpoint").TrimEnd('/') + '/'; } }
            //    public static class Metodos
            //    {
            //        public static string GetFicheiroPauta { get { return getValue("WebServices.SIDE.Metodos.GetFicheiroPauta"); } }
            //        public static string GetPautas { get { return getValue("WebServices.SIDE.Metodos.GetPautas"); } }
            //    }
            //}
            //public static class GesDoc
            //{
            //    public static string Endpoint { get { return getValue("WebServices.GesDoc.Endpoint").TrimEnd('/') + '/'; } }
            //    public static class Metodos
            //    {
            //    }
            //}
            //public static class IAcad
            //{
            //    public static string Endpoint { get { return getValue("WebServices.IAcad.Endpoint").TrimEnd('/') + '/'; } }
            //    public static class Metodos
            //    {
            //        public static string GetPauta { get { return getValue("WebServices.IAcad.Metodos.GetPauta"); } }
            //    }
            //}
        }

        public static class Configuration
        {
            public static class Timer
            {
                public static string Hours
                {
                    get
                    {
                        return getValue("Configuration.Timer.Hours");
                    }
                }
                public static string Minutes
                {
                    get
                    {
                        return getValue("Configuration.Timer.Minutes");
                    }
                }
            }

            public static class Erros
            {
                public static int LogLevel
                {
                    get
                    {
                        try
                        {
                            int level;

                            if (int.TryParse(getValue("Errors.LogLevel"), out level))
                            {
                                return level;
                            }
                        }
                        catch (Exception ex)
                        {
                            var e = new ArgumentException("Não foi possível interpretar o LogLevel no web.config", ex);
                            LogHandling.LogError(e, MaestroStuff.Enums.Erro.Severidade.Critical);
                        }


                        //Warning não deve ser muito verbose, mas manda os erros para trás
                        return 1;
                    }
                }
                public static class Email
                {
                    public static bool Log { get { return (new string[] { "1", "true", "sim", "yes" }).Contains((getValue("Email.LogByEmail") ?? "False").ToLowerInvariant()); } }
                    public static string SMTPServer { get { return getValue("Email.STMPServer"); } }

                    public static string From
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.From");
                        }
                    }
                    public static List<string> To
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.To").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                    public static List<string> CC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.CC").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                    public static List<string> BCC
                    {
                        get
                        {
                            return getValue("Email.ErrorLog.BCC").ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        }
                    }
                }
            }

            public static class WS
            {
                public static int Port
                {
                    get
                    {
                        int port = 0;
                        if (int.TryParse(getValue("WS.Port"), out port))
                        {
                            return port;
                        }
                        else
                        {
                            throw new ArgumentException(string.Format("O valor {0} não é válido para a config WS.Port", getValue("WS.Port")));
                        }
                    }
                }
            }
        }

        public static class ConnectionStrings
        {
            private static string getConnString(string name) { return ConfigurationManager.ConnectionStrings[name].ConnectionString; }
            public static string Maestro { get { return getConnString("devMaestro"); } }
        }
    }
}
/*
 
            public class WS
            {
                public static string Url { get { return @"http://localhost:1537/Acad/"; } }
                public static string GetPautaMethodSignature { get { return @"GetPauta?numPauta={0}"; } }
            }
 
 
 */