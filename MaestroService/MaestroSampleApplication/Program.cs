﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaestroStuff.Notifications;
using MaestroStuff.Notifications;

namespace MaestroSampleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            MaestroNotifier.ConsoleNotifier.WriteToConsole_Info("Início");

            try
            {
                string nullReferenceException = null;

                nullReferenceException.ToString();
            }
            catch (Exception ex)
            {
                MaestroNotifier.ConsoleNotifier.WriteToConsole(ex);
            }

            MaestroNotifier.ConsoleNotifier.WriteToConsole_End("Fim da exec");
        }
    }
}
