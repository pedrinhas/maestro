USE [maestro]
GO
/****** Object:  User [INTRA\sis-dev]    Script Date: 20/03/2018 14:50:45 PM ******/
CREATE USER [INTRA\sis-dev] FOR LOGIN [INTRA\sis-dev] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_executer] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INTRA\sis-dev]
GO
