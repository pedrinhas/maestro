USE [maestro]
GO
/****** Object:  User [winsvc]    Script Date: 20/03/2018 14:50:45 PM ******/
CREATE USER [winsvc] FOR LOGIN [winsvc] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_executer] ADD MEMBER [winsvc]
GO
ALTER ROLE [db_datareader] ADD MEMBER [winsvc]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [winsvc]
GO
