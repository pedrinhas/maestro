USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_I]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_Aplicacao_Log_I]
	@idLog int,
	@data datetime,
	@severidade int,
	@texto nvarchar(max),
	@exClass nvarchar(255),
	@exStackTrace nvarchar(max),
	@exTargetSite nvarchar(max),
	@exMessage nvarchar(max)
	as


	insert into [dbo].[t_Aplicacao_Log] (idLog, data, severidade, texto, exClass, exStackTrace, exTargetSite, exMessage)
	values (@idLog, @data, @severidade, @texto, @exClass, @exStackTrace, @exTargetSite, @exMessage)
GO
