USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_SetIntervalo]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_SetIntervalo]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Parametros_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Parametros_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Parametros_LS]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Parametros_LS]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_LS]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_LS]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Log_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_MinSeveridade_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Log_MinSeveridade_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_I]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Log_I]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_GetNewID]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Log_GetNewID]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_I]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_I]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Historico_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Historico_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_U]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Estado_U]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Estado_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_DatasExecucao_U]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_DatasExecucao_U]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_S]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Config_S]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_LS]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Config_LS]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_I]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP PROCEDURE [dbo].[sp_Aplicacao_Config_I]
GO
ALTER TABLE [dbo].[t_Aplicacao_Parametros] DROP CONSTRAINT [FK_t_Aplicacao_Parametros_t_Aplicacao]
GO
/****** Object:  Table [dbo].[t_Estado_Execucao]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Estado_Execucao]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Tipo]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Tipo]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Parametros]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Parametros]
GO
/****** Object:  Table [dbo].[t_Aplicacao_NotificacoesEmail]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_NotificacoesEmail]
GO
/****** Object:  Table [dbo].[t_Aplicacao_LogExecucoes]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_LogExecucoes]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Log]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Log]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Config_Tipo]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Config_Tipo]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Config]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Config]
GO
/****** Object:  Table [dbo].[t_Aplicacao]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP TABLE [dbo].[t_Aplicacao]
GO
/****** Object:  UserDefinedFunction [dbo].[f_convertTimeToHHMMSS]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP FUNCTION [dbo].[f_convertTimeToHHMMSS]
GO

DECLARE @RoleName sysname
set @RoleName = N'db_executer'

IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [db_executer]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP ROLE [db_executer]
GO
/****** Object:  User [INTRA\sis-dev]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP USER [INTRA\sis-dev]
GO
/****** Object:  User [winsvc]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP USER [winsvc]
GO
USE [master]
GO
/****** Object:  Database [maestro]    Script Date: 20/03/2018 14:44:01 PM ******/
DROP DATABASE [maestro]
GO
/****** Object:  Database [maestro]    Script Date: 20/03/2018 14:44:01 PM ******/
CREATE DATABASE [maestro]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'maestro', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\maestro.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'maestro_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\maestro_log.ldf' , SIZE = 24384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [maestro] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [maestro].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [maestro] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [maestro] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [maestro] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [maestro] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [maestro] SET ARITHABORT OFF 
GO
ALTER DATABASE [maestro] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [maestro] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [maestro] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [maestro] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [maestro] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [maestro] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [maestro] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [maestro] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [maestro] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [maestro] SET  DISABLE_BROKER 
GO
ALTER DATABASE [maestro] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [maestro] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [maestro] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [maestro] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [maestro] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [maestro] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [maestro] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [maestro] SET RECOVERY FULL 
GO
ALTER DATABASE [maestro] SET  MULTI_USER 
GO
ALTER DATABASE [maestro] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [maestro] SET DB_CHAINING OFF 
GO
ALTER DATABASE [maestro] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [maestro] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [maestro] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'maestro', N'ON'
GO
USE [maestro]
GO
/****** Object:  User [winsvc]    Script Date: 20/03/2018 14:44:02 PM ******/
CREATE USER [winsvc] FOR LOGIN [winsvc] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [INTRA\sis-dev]    Script Date: 20/03/2018 14:44:02 PM ******/
CREATE USER [INTRA\sis-dev] FOR LOGIN [INTRA\sis-dev] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [db_executer]    Script Date: 20/03/2018 14:44:02 PM ******/
CREATE ROLE [db_executer]
GO
ALTER ROLE [db_executer] ADD MEMBER [winsvc]
GO
ALTER ROLE [db_datareader] ADD MEMBER [winsvc]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [winsvc]
GO
ALTER ROLE [db_executer] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [INTRA\sis-dev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INTRA\sis-dev]
GO
/****** Object:  UserDefinedFunction [dbo].[f_convertTimeToHHMMSS]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[f_convertTimeToHHMMSS]
(
    @time decimal(28,3), 
    @unit varchar(20)
)
returns varchar(20)
as
begin

    declare @seconds decimal(18,3), @minutes int, @hours int;

    if(@unit = 'hour' or @unit = 'hh' )
        set @seconds = @time * 60 * 60;
    else if(@unit = 'minute' or @unit = 'mi' or @unit = 'n')
        set @seconds = @time * 60;
    else if(@unit = 'second' or @unit = 'ss' or @unit = 's')
        set @seconds = @time;
    else set @seconds = 0; -- unknown time units

    set @hours = convert(int, @seconds /60 / 60);
    set @minutes = convert(int, (@seconds / 60) - (@hours * 60 ));
    set @seconds = @seconds % 60;

    return 
        convert(varchar(9), convert(int, @hours)) + ':' +
        right('00' + convert(varchar(2), convert(int, @minutes)), 2) + ':' +
        right('00' + convert(varchar(6), @seconds), 6)

end
GO
/****** Object:  Table [dbo].[t_Aplicacao]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idTipo] [int] NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[caminhoExe] [nvarchar](max) NOT NULL,
	[intervaloTempo] [nvarchar](30) NULL,
	[estadoExecucao] [int] NOT NULL,
	[ultimaExec] [datetime] NULL,
	[proximaExec] [datetime] NULL,
	[duracaoUltimaExec] [time](1) NULL,
	[ativo] [bit] NULL,
 CONSTRAINT [PK_t_Aplicacao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_Config]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Config](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[tipo] [int] NOT NULL,
	[key] [nvarchar](50) NOT NULL,
	[value] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_Config_Tipo]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Config_Tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_Log]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idLog] [int] NOT NULL,
	[data] [datetime] NULL,
	[severidade] [int] NOT NULL,
	[texto] [nvarchar](max) NOT NULL,
	[exClass] [nvarchar](255) NULL,
	[exStackTrace] [nvarchar](max) NULL,
	[exTargetSite] [nvarchar](max) NULL,
	[exMessage] [nvarchar](max) NULL,
 CONSTRAINT [PK_t_Aplicacao_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_LogExecucoes]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_LogExecucoes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[idLog] [int] NULL,
	[idEstado] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[comentario] [nvarchar](max) NULL,
 CONSTRAINT [PK_t_Aplicacao_LogExecucoes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_NotificacoesEmail]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_NotificacoesEmail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[email] [nvarchar](max) NOT NULL,
	[notificar] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_Parametros]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Parametros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[val] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_t_Aplicacao_Parametros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Aplicacao_Tipo]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_Aplicacao_Tipo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[t_Estado_Execucao]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Estado_Execucao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[nomeFormatado] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[t_Aplicacao_Parametros]  WITH CHECK ADD  CONSTRAINT [FK_t_Aplicacao_Parametros_t_Aplicacao] FOREIGN KEY([idAplicacao])
REFERENCES [dbo].[t_Aplicacao] ([id])
GO
ALTER TABLE [dbo].[t_Aplicacao_Parametros] CHECK CONSTRAINT [FK_t_Aplicacao_Parametros_t_Aplicacao]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_I]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
sp_Aplicacao_Config_S 1, 'WebServices.SIDE.Metodos.GetFicheiroPauta'
*/

create PROCEDURE [dbo].[sp_Aplicacao_Config_I]
	@idAplicacao int,
	@tipo nvarchar(50),
	@key nvarchar(50),
	@value nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if((select COUNT(*)
	from [dbo].[t_Aplicacao_Config]
	where idAplicacao = @idAplicacao
	 and [key] = @key
	 and tipo = @tipo) > 0)
	begin
		--CHAVE EXISTE

		update [dbo].[t_Aplicacao_Config]
		set value = @value
		where idAplicacao = @idAplicacao
		 and [key] = @key
		 and tipo = @tipo
	end
	else
	begin
		--CHAVE NÃO EXISTE

		insert into [dbo].[t_Aplicacao_Config] (idAplicacao, tipo, [key], value)
		values (@idaplicacao, @tipo, @key, @value)
	end	 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_LS]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [dbo].[sp_Aplicacao_Config_LS] 1, 1
*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_Config_LS]
	@idAplicacao int,
	@tipo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select tipo as idTipo, t.nome as tipo, [key], value
	from [dbo].[t_Aplicacao_Config] ac
	left join [dbo].[t_Aplicacao_Config_Tipo] t on t.id = ac.tipo
	where idAplicacao = @idAplicacao and tipo = @tipo
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
sp_Aplicacao_Config_S 1, 'WebServices.SIDE.Metodos.GetFicheiroPauta'
*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_Config_S]
	@idAplicacao int,
	@key nvarchar(50),
	@tipo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select tipo as idTipo, t.nome as tipo, [key], value
	from [dbo].[t_Aplicacao_Config] ac
	left join [dbo].[t_Aplicacao_Config_Tipo] t on t.id = ac.tipo
	where idAplicacao = @idAplicacao and [key] = @key and tipo = @tipo
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_DatasExecucao_U]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec [sp_Aplicacao_Estado_U] 1, 2, 'INICIO'
exec [sp_Aplicacao_Estado_U] 1, 1, 'FIM'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
/*
exec [sp_Aplicacao_Estado_U] 1, 1, 'DISPONIVEL'
exec [sp_Aplicacao_Estado_U] 1, 5, 'PARAR'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
CREATE procedure [dbo].[sp_Aplicacao_DatasExecucao_U]
	@idAplicacao int,
	@ultimaExec datetime,
	@proximaExec datetime = null,
	@duracaoExec time(1)
as
	declare @proximaExecOriginal datetime
	select @proximaExecOriginal = [proximaExec]
	from [dbo].[t_Aplicacao]
	where id = @idAplicacao

	update [dbo].[t_Aplicacao]
	set [ultimaExec] = @ultimaExec, [proximaExec] = COALESCE(@proximaExec, @proximaExecOriginal), [duracaoUltimaExec] = @duracaoExec
	where id = @idAplicacao

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Aplicacao_Estado_S]
	@id int
as
	select a.executar, e.nome, e.nomeFormatado, e.descricao
	from [dbo].[t_Aplicacao] a
	left join [dbo].[t_Estado_Execucao] e on e.id = a.executar
	where a.id = @id
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_U]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec [sp_Aplicacao_Estado_U] 1, 2, 'INICIO'
exec [sp_Aplicacao_Estado_U] 1, 1, 'FIM'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
/*
exec [sp_Aplicacao_Estado_U] 1, 1, 'DISPONIVEL'
exec [sp_Aplicacao_Estado_U] 1, 5, 'PARAR'

exec sp_Aplicacao_Estado_U 6, , 'teste'
*/
CREATE procedure [dbo].[sp_Aplicacao_Estado_U]
	@idAplicacao int,
	@idEstado int,
	@comentario nvarchar(max)
as
	declare @now datetime
			, @lastState int
	declare @idLogs int
	
	set @lastState = (select top 1 estadoExecucao from t_Aplicacao where id = @idAplicacao order by id desc)
	select @idLogs = coalesce(max(idLog), 0) from [dbo].[t_Aplicacao_Log]

	if(@idEstado not in (2, 5) AND not (@idEstado = 1 AND @lastState = 5)) --se estiver no fim da execução
	begin
		insert into [dbo].[t_Aplicacao_LogExecucoes] (idAplicacao, idLog, idEstado, data, comentario)
		values (@idAplicacao, @idLogs, @idEstado, getdate(), @comentario)
	end
	else
	begin
		set @idLogs = @idLogs + 1

		insert into [dbo].[t_Aplicacao_LogExecucoes] (idAplicacao, idLog, idEstado, data, comentario)
		values (@idAplicacao, @idLogs, @idEstado, getdate(), @comentario)
	end

	update [dbo].[t_Aplicacao]
	set [estadoExecucao] = @idEstado
	where id = @idAplicacao
	

	select @idLogs as idLog
	
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Historico_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Aplicacao_Historico_S]
	@id int
	as 
	
SELECT h.[id] as idAplicacaoHistorico
      ,[idAplicacao]
	  ,[idLog]
	  , a.nome as nomeAplicacao
      ,[idEstado]
	  , e.nome as nomeEstado
	  , e.nomeFormatado as nomeFormatado
	  , e.descricao as descricao
      ,[data]
      ,[comentario]
  FROM [dbo].[t_Aplicacao_LogExecucoes] h
  left join t_Aplicacao a on a.id = h.idAplicacao
  left join t_Estado_Execucao e on e.id = h.idEstado
  where [idAplicacao] = @id
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_I]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [sp_Aplicacao_I] 4, 'teste', 'esta é uma aplicação teste', 'c:\oi\aiaiai.exe', ' 10 2 * * *'

*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_I]
	  @id int
	, @tipo int
	, @nome nvarchar(50)
	, @descricao nvarchar(max)
	, @caminhoExe nvarchar(max)
	, @intervaloTempo nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id > 0)
	begin
		update [t_Aplicacao]
		set idtipo = @tipo, nome = @nome, descricao = @descricao, caminhoExe = @caminhoExe, intervaloTempo = @intervaloTempo
		where id = @id
	end
	else
	begin
		insert into [dbo].[t_Aplicacao] (idtipo, nome, descricao, caminhoExe, intervaloTempo, estadoExecucao, ativo)
		values (@tipo, @nome, @descricao, @caminhoExe, @intervaloTempo, 1, 1)
	end

	select ISNULL(SCOPE_IDENTITY(), @id)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_GetNewID]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- [sp_Aplicacao_Log_GetNewID] 0

CREATE PROCEDURE [dbo].[sp_Aplicacao_Log_GetNewID]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Por causa da l 154 do ficheiro Aplicacao.cs do serviço, que chama o SP sp_Aplicacao_Estado_U. O novo idLog é dado na SP sp_Aplicacao_Estado_U, e aqui apenas tem de ser devolvido, pois é novo
	select coalesce(max(idLog), 0) + 1 from [dbo].[t_Aplicacao_Log]
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_I]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[sp_Aplicacao_Log_I]
	@idLog int,
	@data datetime,
	@severidade int,
	@texto nvarchar(max),
	@exClass nvarchar(255),
	@exStackTrace nvarchar(max),
	@exTargetSite nvarchar(max),
	@exMessage nvarchar(max)
	as


	insert into [dbo].[t_Aplicacao_Log] (idLog, data, severidade, texto, exClass, exStackTrace, exTargetSite, exMessage)
	values (@idLog, @data, @severidade, @texto, @exClass, @exStackTrace, @exTargetSite, @exMessage)
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_MinSeveridade_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
[dbo].[sp_Aplicacao_Log_S] 1009

*/
create PROCEDURE [dbo].[sp_Aplicacao_Log_MinSeveridade_S]
	@minSeveridade int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select distinct l.id as idLogExec, a.id as idAplicacao, t.Nome as Tipo, a.Nome, a.IntervaloTempo, a.EstadoExecucao, a.UltimaExec, a.ProximaExec, a.Ativo, l.idLog, l.Data, l.Severidade, l.Texto, l.exClass, l.exStackTrace, l.exTargetSite, l.exMessage
	from [dbo].[t_Aplicacao_LogExecucoes] e
	left join [dbo].[t_Aplicacao] a on a.id = e.idAplicacao
	left join [dbo].[t_Aplicacao_Log] l on l.idLog = e.idLog
	left join [dbo].[t_Aplicacao_Tipo] t on t.id = a.idTipo
	where l.severidade >= @minSeveridade 
	order by l.Data
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
[dbo].[sp_Aplicacao_Log_S] 11
[dbo].[sp_Aplicacao_Log_S] 1009
[dbo].[sp_Aplicacao_Log_S] 2009
[dbo].[sp_Aplicacao_Log_S] 2010
select * from [dbo].[t_Aplicacao_Log] where idlog = 118
*/
CREATE PROCEDURE [dbo].[sp_Aplicacao_Log_S] 
	@idAplicacao int
AS
BEGIN
	SET NOCOUNT ON;

select distinct l.id as idLogExec, a.id as idAplicacao, t.Nome as Tipo, a.Nome, a.IntervaloTempo, a.EstadoExecucao, a.UltimaExec, a.ProximaExec, a.Ativo, l.idLog, l.Data, l.Severidade, l.Texto, l.exClass, l.exStackTrace, l.exTargetSite, l.exMessage
	from [dbo].[t_Aplicacao_LogExecucoes] e
	left join [dbo].[t_Aplicacao] a on a.id = e.idAplicacao
	left join [dbo].[t_Aplicacao_Log] l on l.idLog = e.idLog
	left join [dbo].[t_Aplicacao_Tipo] t on t.id = a.idTipo
	where e.[idAplicacao] = @idAplicacao
	and l.data > '2018-01-16 04:00:01.000'
	order by l.Data 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_LS]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Aplicacao_LS]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT a.id, t.id as idTipo, t.nome as tipo, t.descricao as tipoDescricao, a.nome, a.descricao, a.caminhoExe, a.intervaloTempo, a.estadoExecucao, a.ultimaExec, a.proximaExec, a.duracaoUltimaExec, a.ativo
	FROM t_Aplicacao a
	left join [dbo].[t_Aplicacao_Tipo] t on t.id = a.idTipo
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Parametros_LS]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Aplicacao_Parametros_LS]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT id, idAplicacao, val
	from t_Aplicacao_Parametros
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Parametros_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Aplicacao_Parametros_S]
	@idAplicacao int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT id, idAplicacao, val
	from t_Aplicacao_Parametros
	where idAplicacao = @idAplicacao
END



GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_S]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/*
[dbo].[sp_Aplicacao_S] 6

*/
CREATE PROCEDURE [dbo].[sp_Aplicacao_S]
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT a.id, a.nome, a.descricao, a.caminhoExe, a.intervaloTempo, a.estadoExecucao, a.ultimaExec, a.proximaExec, a.duracaoUltimaExec, a.ativo,
	t.id, t.nome, t.descricao
	FROM t_Aplicacao a
	left join t_Aplicacao_Tipo t on a.idTipo = t.id
	where a.id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_SetIntervalo]    Script Date: 20/03/2018 14:44:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [dbo].[sp_Aplicacao_Config_LS] 1, 1
*/

create PROCEDURE [dbo].[sp_Aplicacao_SetIntervalo]
	@idAplicacao int,
	@intervalo nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update [dbo].[t_Aplicacao]
	set [intervaloTempo] = @intervalo
	where id = @idAplicacao
END

GO
USE [master]
GO
ALTER DATABASE [maestro] SET  READ_WRITE 
GO



USE [maestro]
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config_Tipo] ON 

GO
INSERT [dbo].[t_Aplicacao_Config_Tipo] ([id], [nome], [descricao]) VALUES (1, N'appSettings', N'Configurações gerais da aplicação')
GO
INSERT [dbo].[t_Aplicacao_Config_Tipo] ([id], [nome], [descricao]) VALUES (2, N'connectionStrings', N'Connection strings')
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config_Tipo] OFF
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_NotificacoesEmail] ON 

GO
INSERT [dbo].[t_Aplicacao_NotificacoesEmail] ([id], [idAplicacao], [email], [notificar]) VALUES (1, 1, N'cpereira@utad.pt', 1)
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_NotificacoesEmail] OFF
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Tipo] ON 

GO
INSERT [dbo].[t_Aplicacao_Tipo] ([id], [nome], [descricao]) VALUES (1, N'Executavel', N'Ficheiro executável para correr a partir do servidor')
GO
INSERT [dbo].[t_Aplicacao_Tipo] ([id], [nome], [descricao]) VALUES (2, N'Web', N'Endereço web para fazer um pedido')
GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Tipo] OFF
GO
SET IDENTITY_INSERT [dbo].[t_Estado_Execucao] ON 

GO
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (1, N'disponivel', N'Disponível', N'A aplicação está disponível para ser executada.')
GO
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (2, N'emExecucao', N'Em execução', N'A aplicação está a ser executada neste momento. Não deverá ser iniciada.')
GO
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (3, N'erro', N'Erro', N'Ocorreram erros na última execução da aplicação. É necessário averiguar o que se passou.')
GO
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (4, N'erroCritico', N'Erro crítico', N'A última execução da aplicação resultou em erros críticos. Não deverá ser iniciada até que se perceba o que aconteceu.')
GO
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (5, N'parada', N'Parada', N'A aplicação não será executada.')
GO
SET IDENTITY_INSERT [dbo].[t_Estado_Execucao] OFF
GO
