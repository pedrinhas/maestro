USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Aplicacao_Estado_S]
	@id int
as
	select a.executar, e.nome, e.nomeFormatado, e.descricao
	from [dbo].[t_Aplicacao] a
	left join [dbo].[t_Estado_Execucao] e on e.id = a.executar
	where a.id = @id
GO
