USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_NotificacoesEmail]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_NotificacoesEmail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[email] [nvarchar](max) NOT NULL,
	[notificar] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_NotificacoesEmail] ON 

INSERT [dbo].[t_Aplicacao_NotificacoesEmail] ([id], [idAplicacao], [email], [notificar]) VALUES (1, 1, N'cpereira@utad.pt', 1)
SET IDENTITY_INSERT [dbo].[t_Aplicacao_NotificacoesEmail] OFF
