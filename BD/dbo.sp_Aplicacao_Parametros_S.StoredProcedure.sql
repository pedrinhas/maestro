USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Parametros_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Aplicacao_Parametros_S]
	@idAplicacao int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT id, idAplicacao, val
	from t_Aplicacao_Parametros
	where idAplicacao = @idAplicacao
END



GO
