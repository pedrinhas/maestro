USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_DatasExecucao_U]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec [sp_Aplicacao_Estado_U] 1, 2, 'INICIO'
exec [sp_Aplicacao_Estado_U] 1, 1, 'FIM'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
/*
exec [sp_Aplicacao_Estado_U] 1, 1, 'DISPONIVEL'
exec [sp_Aplicacao_Estado_U] 1, 5, 'PARAR'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
CREATE procedure [dbo].[sp_Aplicacao_DatasExecucao_U]
	@idAplicacao int,
	@ultimaExec datetime,
	@proximaExec datetime = null,
	@duracaoExec time(1)
as
	declare @proximaExecOriginal datetime
	select @proximaExecOriginal = [proximaExec]
	from [dbo].[t_Aplicacao]
	where id = @idAplicacao

	update [dbo].[t_Aplicacao]
	set [ultimaExec] = @ultimaExec, [proximaExec] = COALESCE(@proximaExec, @proximaExecOriginal), [duracaoUltimaExec] = @duracaoExec
	where id = @idAplicacao

GO
