USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_MinSeveridade_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
[dbo].[sp_Aplicacao_Log_S] 1009

*/
create PROCEDURE [dbo].[sp_Aplicacao_Log_MinSeveridade_S]
	@minSeveridade int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select distinct l.id as idLogExec, a.id as idAplicacao, t.Nome as Tipo, a.Nome, a.IntervaloTempo, a.EstadoExecucao, a.UltimaExec, a.ProximaExec, a.Ativo, l.idLog, l.Data, l.Severidade, l.Texto, l.exClass, l.exStackTrace, l.exTargetSite, l.exMessage
	from [dbo].[t_Aplicacao_LogExecucoes] e
	left join [dbo].[t_Aplicacao] a on a.id = e.idAplicacao
	left join [dbo].[t_Aplicacao_Log] l on l.idLog = e.idLog
	left join [dbo].[t_Aplicacao_Tipo] t on t.id = a.idTipo
	where l.severidade >= @minSeveridade 
	order by l.Data
END


GO
