USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Log_GetNewID]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- [sp_Aplicacao_Log_GetNewID] 0

CREATE PROCEDURE [dbo].[sp_Aplicacao_Log_GetNewID]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Por causa da l 154 do ficheiro Aplicacao.cs do serviço, que chama o SP sp_Aplicacao_Estado_U. O novo idLog é dado na SP sp_Aplicacao_Estado_U, e aqui apenas tem de ser devolvido, pois é novo
	select coalesce(max(idLog), 0) + 1 from [dbo].[t_Aplicacao_Log]
END

GO
