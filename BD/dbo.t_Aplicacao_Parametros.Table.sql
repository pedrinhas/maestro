USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Parametros]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Parametros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[val] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_t_Aplicacao_Parametros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[t_Aplicacao_Parametros]  WITH CHECK ADD  CONSTRAINT [FK_t_Aplicacao_Parametros_t_Aplicacao] FOREIGN KEY([idAplicacao])
REFERENCES [dbo].[t_Aplicacao] ([id])
GO
ALTER TABLE [dbo].[t_Aplicacao_Parametros] CHECK CONSTRAINT [FK_t_Aplicacao_Parametros_t_Aplicacao]
GO
