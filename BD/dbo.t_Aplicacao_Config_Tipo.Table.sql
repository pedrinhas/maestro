USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Config_Tipo]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Config_Tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config_Tipo] ON 

INSERT [dbo].[t_Aplicacao_Config_Tipo] ([id], [nome], [descricao]) VALUES (1, N'appSettings', N'Configurações gerais da aplicação')
INSERT [dbo].[t_Aplicacao_Config_Tipo] ([id], [nome], [descricao]) VALUES (2, N'connectionStrings', N'Connection strings')
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config_Tipo] OFF
