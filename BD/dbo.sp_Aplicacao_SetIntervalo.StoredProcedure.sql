USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_SetIntervalo]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [dbo].[sp_Aplicacao_Config_LS] 1, 1
*/

create PROCEDURE [dbo].[sp_Aplicacao_SetIntervalo]
	@idAplicacao int,
	@intervalo nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update [dbo].[t_Aplicacao]
	set [intervaloTempo] = @intervalo
	where id = @idAplicacao
END

GO
