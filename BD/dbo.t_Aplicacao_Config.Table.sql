USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Config]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Config](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[tipo] [int] NOT NULL,
	[key] [nvarchar](50) NOT NULL,
	[value] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config] ON 

INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (1, 1, 2, N'SIGAcad', N'asdasdasd')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (3, 1, 2, N'Intermedia', N'asdasdasd')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (4, 1, 2, N'IntermediaAcademSeg', N'Data Source=apps2.utad.pt;Initial Catalog=inscricoes2SIGACAD; Application Name=IAcad; Integrated Security=False;User ID=cpereira; Password=p4s7i10')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (5, 1, 1, N'WebServices.SIDE.Endpoint', N'http://192.168.111.221:80/PhpProjectRestDEV/index.php/')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (6, 1, 1, N'WebServices.SIDE.Metodos.GetFicheiroPauta', N'getPautaTXT/{0}')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (7, 1, 1, N'WebServices.SIDE.Metodos.GetPautas', N'getPautasLacradasDataHora/{0}/{1}')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (8, 1, 1, N'WebServices.GesDoc.Endpoint', N'asdasd')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (9, 1, 1, N'WebServices.IAcad.Endpoint', N'http://localhost:1537/Acad/')
INSERT [dbo].[t_Aplicacao_Config] ([id], [idAplicacao], [tipo], [key], [value]) VALUES (10, 1, 1, N'WebServices.IAcad.Metodos.GetPauta', N'GetPauta?numPauta={0}')
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Config] OFF
