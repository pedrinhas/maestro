USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Log]    Script Date: 20/03/2018 14:50:11 PM ******/
DROP TABLE [dbo].[t_Aplicacao_Log]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Log]    Script Date: 20/03/2018 14:50:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idLog] [int] NOT NULL,
	[data] [datetime] NULL,
	[severidade] [int] NOT NULL,
	[texto] [nvarchar](max) NOT NULL,
	[exClass] [nvarchar](255) NULL,
	[exStackTrace] [nvarchar](max) NULL,
	[exTargetSite] [nvarchar](max) NULL,
	[exMessage] [nvarchar](max) NULL,
 CONSTRAINT [PK_t_Aplicacao_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
