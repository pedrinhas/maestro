USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/*
[dbo].[sp_Aplicacao_S] 6

*/
CREATE PROCEDURE [dbo].[sp_Aplicacao_S]
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT a.id, a.nome, a.descricao, a.caminhoExe, a.intervaloTempo, a.estadoExecucao, a.ultimaExec, a.proximaExec, a.duracaoUltimaExec, a.ativo,
	t.id, t.nome, t.descricao
	FROM t_Aplicacao a
	left join t_Aplicacao_Tipo t on a.idTipo = t.id
	where a.id = @id
END

GO
