USE [maestro]
GO
/****** Object:  Table [dbo].[t_Estado_Execucao]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Estado_Execucao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[nomeFormatado] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Estado_Execucao] ON 

INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (1, N'disponivel', N'Disponível', N'A aplicação está disponível para ser executada.')
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (2, N'emExecucao', N'Em execução', N'A aplicação está a ser executada neste momento. Não deverá ser iniciada.')
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (3, N'erro', N'Erro', N'Ocorreram erros na última execução da aplicação. É necessário averiguar o que se passou.')
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (4, N'erroCritico', N'Erro crítico', N'A última execução da aplicação resultou em erros críticos. Não deverá ser iniciada até que se perceba o que aconteceu.')
INSERT [dbo].[t_Estado_Execucao] ([id], [nome], [nomeFormatado], [descricao]) VALUES (5, N'parada', N'Parada', N'A aplicação não será executada.')
SET IDENTITY_INSERT [dbo].[t_Estado_Execucao] OFF
