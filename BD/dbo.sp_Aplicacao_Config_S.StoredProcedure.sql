USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
sp_Aplicacao_Config_S 1, 'WebServices.SIDE.Metodos.GetFicheiroPauta'
*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_Config_S]
	@idAplicacao int,
	@key nvarchar(50),
	@tipo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select tipo as idTipo, t.nome as tipo, [key], value
	from [dbo].[t_Aplicacao_Config] ac
	left join [dbo].[t_Aplicacao_Config_Tipo] t on t.id = ac.tipo
	where idAplicacao = @idAplicacao and [key] = @key and tipo = @tipo
END

GO
