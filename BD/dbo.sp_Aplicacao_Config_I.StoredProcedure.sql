USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_I]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
sp_Aplicacao_Config_S 1, 'WebServices.SIDE.Metodos.GetFicheiroPauta'
*/

create PROCEDURE [dbo].[sp_Aplicacao_Config_I]
	@idAplicacao int,
	@tipo nvarchar(50),
	@key nvarchar(50),
	@value nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if((select COUNT(*)
	from [dbo].[t_Aplicacao_Config]
	where idAplicacao = @idAplicacao
	 and [key] = @key
	 and tipo = @tipo) > 0)
	begin
		--CHAVE EXISTE

		update [dbo].[t_Aplicacao_Config]
		set value = @value
		where idAplicacao = @idAplicacao
		 and [key] = @key
		 and tipo = @tipo
	end
	else
	begin
		--CHAVE NÃO EXISTE

		insert into [dbo].[t_Aplicacao_Config] (idAplicacao, tipo, [key], value)
		values (@idaplicacao, @tipo, @key, @value)
	end	 
END

GO
