USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_I]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [sp_Aplicacao_I] 4, 'teste', 'esta é uma aplicação teste', 'c:\oi\aiaiai.exe', ' 10 2 * * *'

*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_I]
	  @id int
	, @tipo int
	, @nome nvarchar(50)
	, @descricao nvarchar(max)
	, @caminhoExe nvarchar(max)
	, @intervaloTempo nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id > 0)
	begin
		update [t_Aplicacao]
		set idtipo = @tipo, nome = @nome, descricao = @descricao, caminhoExe = @caminhoExe, intervaloTempo = @intervaloTempo
		where id = @id
	end
	else
	begin
		insert into [dbo].[t_Aplicacao] (idtipo, nome, descricao, caminhoExe, intervaloTempo, estadoExecucao, ativo)
		values (@tipo, @nome, @descricao, @caminhoExe, @intervaloTempo, 1, 1)
	end

	select ISNULL(SCOPE_IDENTITY(), @id)
END

GO
