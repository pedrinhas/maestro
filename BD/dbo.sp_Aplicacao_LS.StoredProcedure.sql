USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_LS]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Aplicacao_LS]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT a.id, t.id as idTipo, t.nome as tipo, t.descricao as tipoDescricao, a.nome, a.descricao, a.caminhoExe, a.intervaloTempo, a.estadoExecucao, a.ultimaExec, a.proximaExec, a.duracaoUltimaExec, a.ativo
	FROM t_Aplicacao a
	left join [dbo].[t_Aplicacao_Tipo] t on t.id = a.idTipo
END

GO
