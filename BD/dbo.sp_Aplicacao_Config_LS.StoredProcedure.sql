USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Config_LS]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

/*
exec [dbo].[sp_Aplicacao_Config_LS] 1, 1
*/

CREATE PROCEDURE [dbo].[sp_Aplicacao_Config_LS]
	@idAplicacao int,
	@tipo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select tipo as idTipo, t.nome as tipo, [key], value
	from [dbo].[t_Aplicacao_Config] ac
	left join [dbo].[t_Aplicacao_Config_Tipo] t on t.id = ac.tipo
	where idAplicacao = @idAplicacao and tipo = @tipo
END

GO
