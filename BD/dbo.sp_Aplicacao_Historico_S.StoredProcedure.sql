USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Historico_S]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Aplicacao_Historico_S]
	@id int
	as 
	
SELECT h.[id] as idAplicacaoHistorico
      ,[idAplicacao]
	  ,[idLog]
	  , a.nome as nomeAplicacao
      ,[idEstado]
	  , e.nome as nomeEstado
	  , e.nomeFormatado as nomeFormatado
	  , e.descricao as descricao
      ,[data]
      ,[comentario]
  FROM [dbo].[t_Aplicacao_LogExecucoes] h
  left join t_Aplicacao a on a.id = h.idAplicacao
  left join t_Estado_Execucao e on e.id = h.idEstado
  where [idAplicacao] = @id
GO
