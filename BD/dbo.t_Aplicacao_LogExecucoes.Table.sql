USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_LogExecucoes]    Script Date: 20/03/2018 14:50:11 PM ******/
DROP TABLE [dbo].[t_Aplicacao_LogExecucoes]
GO
/****** Object:  Table [dbo].[t_Aplicacao_LogExecucoes]    Script Date: 20/03/2018 14:50:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_LogExecucoes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAplicacao] [int] NOT NULL,
	[idLog] [int] NULL,
	[idEstado] [int] NOT NULL,
	[data] [datetime] NOT NULL,
	[comentario] [nvarchar](max) NULL,
 CONSTRAINT [PK_t_Aplicacao_LogExecucoes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
