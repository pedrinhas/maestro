USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idTipo] [int] NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](max) NULL,
	[caminhoExe] [nvarchar](max) NOT NULL,
	[intervaloTempo] [nvarchar](30) NULL,
	[estadoExecucao] [int] NOT NULL,
	[ultimaExec] [datetime] NULL,
	[proximaExec] [datetime] NULL,
	[duracaoUltimaExec] [time](1) NULL,
	[ativo] [bit] NULL,
 CONSTRAINT [PK_t_Aplicacao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao] ON 

INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (1, 1, N'Pautas', N'Serviço de importação de pautas alterado', N'a:\cena.bat', N'00 00 00 * * ?', 1, CAST(N'2017-05-10 17:08:15.043' AS DateTime), CAST(N'2017-05-10 17:08:30.000' AS DateTime), NULL, 0)
INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (11, 1, N'Requerimentos', N'Requerimentos - consultas ao GesDoc', N'C:\Program Files (x86)\UTAD\Requerimentos\RequerimentosMaestro.exe', N'00 00 01 * * ?', 1, CAST(N'2018-03-20 01:00:03.957' AS DateTime), CAST(N'2018-03-21 01:00:00.000' AS DateTime), CAST(N'00:00:03.9000000' AS Time), 1)
INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (1009, 1, N'RCU', N'Sincronizador do RCU (prod)', N'C:\Program Files (x86)\UTAD\RCUSynchronizer\RCUMaestroSynchronizer.exe', N'30 42 00 * * ?', 1, CAST(N'2018-03-20 14:41:32.817' AS DateTime), CAST(N'2018-03-21 00:42:30.000' AS DateTime), CAST(N'00:00:13.4000000' AS Time), 1)
INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (2009, 1, N'Sincronizar Docentes RCU INTRA', N'Aplicação de sincronização da lista "Docentes" do SharePoint da INTRA', N'C:\Program Files (x86)\UTAD\RCUSharePointSynchronizer\RCUSharePointMaestroSynchronizer.exe', N'00 30 01 * * ?', 1, CAST(N'2018-02-16 01:30:03.057' AS DateTime), CAST(N'2018-02-17 01:30:00.000' AS DateTime), CAST(N'00:00:03.1000000' AS Time), 0)
INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (2010, 1, N'Bolsas de Investigação', N'Bolsas de Investigação', N'C:\Program Files (x86)\UTAD\BolsasInvestigacaoMaestro\BolsasInvestigacaoMaestro.exe', N'00 20 01 * * ?', 1, CAST(N'2018-03-15 01:20:00.627' AS DateTime), CAST(N'2018-03-16 01:20:00.000' AS DateTime), CAST(N'00:00:00.6000000' AS Time), 0)
INSERT [dbo].[t_Aplicacao] ([id], [idTipo], [nome], [descricao], [caminhoExe], [intervaloTempo], [estadoExecucao], [ultimaExec], [proximaExec], [duracaoUltimaExec], [ativo]) VALUES (2012, 1, N'Teste', N'Testes locais', N'A:\oi.bat', N'00 00 00 01 * ?', 1, CAST(N'2018-03-19 16:19:29.267' AS DateTime), CAST(N'2018-03-20 16:19:00.000' AS DateTime), CAST(N'00:00:00.2000000' AS Time), 1)
SET IDENTITY_INSERT [dbo].[t_Aplicacao] OFF
