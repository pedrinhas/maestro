USE [maestro]
GO
/****** Object:  StoredProcedure [dbo].[sp_Aplicacao_Estado_U]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec [sp_Aplicacao_Estado_U] 1, 2, 'INICIO'
exec [sp_Aplicacao_Estado_U] 1, 1, 'FIM'

exec [dbo].[sp_Aplicacao_Estado_S] 1
exec [dbo].[sp_Aplicacao_Historico_S] 1
*/
/*
exec [sp_Aplicacao_Estado_U] 1, 1, 'DISPONIVEL'
exec [sp_Aplicacao_Estado_U] 1, 5, 'PARAR'

exec sp_Aplicacao_Estado_U 6, , 'teste'
*/
CREATE procedure [dbo].[sp_Aplicacao_Estado_U]
	@idAplicacao int,
	@idEstado int,
	@comentario nvarchar(max)
as
	declare @now datetime
			, @lastState int
	declare @idLogs int
	
	set @lastState = (select top 1 estadoExecucao from t_Aplicacao where id = @idAplicacao order by id desc)
	select @idLogs = coalesce(max(idLog), 0) from [dbo].[t_Aplicacao_Log]

	if(@idEstado not in (2, 5) AND not (@idEstado = 1 AND @lastState = 5)) --se estiver no fim da execução
	begin
		insert into [dbo].[t_Aplicacao_LogExecucoes] (idAplicacao, idLog, idEstado, data, comentario)
		values (@idAplicacao, @idLogs, @idEstado, getdate(), @comentario)
	end
	else
	begin
		set @idLogs = @idLogs + 1

		insert into [dbo].[t_Aplicacao_LogExecucoes] (idAplicacao, idLog, idEstado, data, comentario)
		values (@idAplicacao, @idLogs, @idEstado, getdate(), @comentario)
	end

	update [dbo].[t_Aplicacao]
	set [estadoExecucao] = @idEstado
	where id = @idAplicacao
	

	select @idLogs as idLog
	
GO
