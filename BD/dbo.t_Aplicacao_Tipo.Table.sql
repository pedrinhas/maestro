USE [maestro]
GO
/****** Object:  Table [dbo].[t_Aplicacao_Tipo]    Script Date: 20/03/2018 14:50:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_Aplicacao_Tipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](50) NOT NULL,
	[descricao] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_Aplicacao_Tipo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Tipo] ON 

INSERT [dbo].[t_Aplicacao_Tipo] ([id], [nome], [descricao]) VALUES (1, N'Executavel', N'Ficheiro executável para correr a partir do servidor')
INSERT [dbo].[t_Aplicacao_Tipo] ([id], [nome], [descricao]) VALUES (2, N'Web', N'Endereço web para fazer um pedido')
SET IDENTITY_INSERT [dbo].[t_Aplicacao_Tipo] OFF
